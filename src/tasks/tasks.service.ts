import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { v1 as uuid } from 'uuid';
import { createTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';


@Injectable()
export class TasksService {

  constructor(
    @InjectRepository(TaskRepository)
    private taskRepository: TaskRepository,
  ) {}

  async getTaskById(id: string): Promise<Task> {
    const found = await this.taskRepository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }

    return found;
  }

  async createTask(createTaskDto : createTaskDto): Promise<Task> {
    const task: Task = await this.taskRepository.createTask(createTaskDto);
    return task;       
}

    private tasks: Task[] = [];
    async getAllTasks(): Promise<Task[]> {
        this.tasks = await this.taskRepository.getAllTasks();
        return this.tasks;
    }

    async deleteTaskById(id: string): Promise<Task[]>{
        if (this.getTaskById(id)) {
            await this.taskRepository.deleteTask(id);
        }
        return this.tasks;
    }

    async updateTaskStatusById(id: string, status: TaskStatus): Promise<Task>{
        const found : Task = await this.getTaskById(id);
        found.status = status;
        await found.save();
        return found;
    }

    // private tasks: Task[] = [];

    // getAllTasks(): Task[] {
    //     return this.tasks
    // }

    // createTask(createTask : createTaskDto): Task {
    //     const task: Task = {
    //         id: uuid(),
    //         title: createTask.title,
    //         description: createTask.description,
    //         status: TaskStatus.OPEN
    //     }
    //     this.tasks.push(task);
    //     return task        
    // }

    // getTaskById(id: string): Task{
    //     const task: Task = this.tasks.find((task) => task.id === id);
    //     if (!task) {
    //         throw new NotFoundException();
    //     }    
    //     return task;
    // }

    // deleteTaskById(id: string): Task[]{
    //     if (this.getTaskById(id)) {
    //         this.tasks = this.tasks.filter((task) => task.id !== id);
    //     }
    //     return this.tasks;
    // }

    // updateTaskStatusById(id: string, status: TaskStatus): Task{
    //     const task : Task = this.getTaskById(id);
    //     task.status = status;
    //     return task;
    // }

    // filterTask(status: TaskStatus, keyword: string): Task[]{
    //     const tasks = this.tasks.filter(
    //         (task) => 
    //             task.status===status ||
    //             (task.title.includes(keyword) || task.description.includes(keyword))
    //         )
    //     return tasks
    // }

}
