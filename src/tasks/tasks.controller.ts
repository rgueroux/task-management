import { Controller, Get, Post, Param, Body, Query, Delete, Put, Patch, UsePipes, ValidationPipe } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { Task } from './task.entity';
import { createTaskDto } from './dto/create-task.dto';
import { Validator } from 'class-validator';
import { TaskStatus } from './task-status.enum';

@Controller('tasks')
export class TasksController {

    constructor(private tasksService: TasksService) {}

    @Get()
    async getAllTasks(
        @Query('status') status: TaskStatus,
        @Query('keyword') keyword: string
    ): Promise<Task[]> {
        if (status || keyword) {
            // return this.tasksService.filterTask(status, keyword);    
        } else {
            return this.tasksService.getAllTasks();
        }       
    }
    
    @Get('/:id')
    async getTaskById(@Param('id') id: string): Promise<Task> {
        return this.tasksService.getTaskById(id);
    }

    @Post()
    @UsePipes(ValidationPipe)
    async createTask(
      @Body() createTaskDto: createTaskDto
    ): Promise<Task> {
      return this.tasksService.createTask(createTaskDto);
    }

    @Delete(':id')
    async deleteTaskById(
        @Param('id') id: string
    ): Promise<Task[]> {
        return this.tasksService.deleteTaskById(id);
    }

    @Patch(':id')
    async updateTaskStatusById(
        @Param('id') id: string,
        @Body('status') status: TaskStatus
    ): Promise<Task> {
        return this.tasksService.updateTaskStatusById(id, status);
    }

    // @Get()
    // getAllTasks(
    //     @Query('status') status: TaskStatus,
    //     @Query('keyword') keyword: string
    // ): Task[] {
    //     if (status || keyword) {
    //         return this.tasksService.filterTask(status, keyword);    
    //     } else {
    //         return this.tasksService.getAllTasks();
    //     }        
    // }

    // @Post()
    // @UsePipes(ValidationPipe)
    // createTask(
    //   @Body() createTaskDto: createTaskDto
    // ): Task {
    //   return this.tasksService.createTask(createTaskDto);
    // }

    // @Get(':id')
    // getTaskById(
    //     @Param('id') id: string
    // ): Task {
    //     return this.tasksService.getTaskById(id);
    // }

    // @Delete(':id')
    // deleteTaskById(
    //     @Param('id') id: string
    // ): Task[] {
    //     return this.tasksService.deleteTaskById(id);
    // }

    // @Patch(':id')
    // updateTaskStatusById(
    //     @Param('id') id: string,
    //     @Param('status') status: TaskStatus
    // ): Task {
    //     return this.tasksService.updateTaskStatusById(id, status);
    // }

}
