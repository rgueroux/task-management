import { Body, Controller, Get, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/get-user.decorator';
import { Auth } from './auth.entity';
import { AuthmoduleService } from './authmodule.service';
import { createUserDto } from './dto/create-user.dto';
import { JwtPayload } from './jwtPayload.model';

@Controller('authmodule')
export class AuthmoduleController {

    constructor(private authService: AuthmoduleService, private jwtService: JwtService) {}

    @Post()
    @UsePipes(ValidationPipe)
    async createUser(
      @Body() createUserDto : createUserDto
    ): Promise<Auth> {
      return this.authService.createUser(createUserDto);
    }

    @Post('signin')
    signin(
      @Body('username') username: string
    ) {
      const payload: JwtPayload = { username };
      const accessToken = this.jwtService.sign(payload);
      return { accessToken };
    }


    @Get('monprofil')
    @UseGuards(AuthGuard("jwt"))
    getMyProfile(@GetUser() user : Auth): Promise<Auth>{
      return this.authService.getUserById(user.id);
    }

  }
