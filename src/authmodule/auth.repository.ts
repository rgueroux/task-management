import { EntityRepository, Repository } from "typeorm";
import { Auth } from "./auth.entity";
import { createUserDto } from './dto/create-user.dto';
import * as bcrypt from 'bcrypt';

@EntityRepository(Auth)
export class AuthRepository extends Repository<Auth> {

    async createUser(createUserDto: createUserDto): Promise<Auth> {
        const { username, password } = createUserDto;
        
        const user = new Auth();
        user.username = username;
        user.password = await bcrypt.hash(password, 10);
        await user.save();

        return user;
    }
}
