import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Auth } from './auth.entity';
import { AuthRepository } from './auth.repository';
import { createUserDto } from './dto/create-user.dto';

@Injectable()
export class AuthmoduleService {

    constructor(
        @InjectRepository(AuthRepository)
        private authRepository: AuthRepository,
      ) {}


    async createUser(createUserDto : createUserDto): Promise<Auth> {
        const task: Auth = await this.authRepository.createUser(createUserDto);
        return task;       
    }

    async getUserById(id: number): Promise<Auth> {
        const found = await this.authRepository.findOne(id);
        if (!found) {
          throw new NotFoundException(`Task with ID "${id}" not found`);
        }
    
        return found;
      }
}
