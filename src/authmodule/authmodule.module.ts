import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtStrategy } from 'src/jwt.strategy';
import { AuthRepository } from './auth.repository';
import { AuthmoduleController } from './authmodule.controller';
import { AuthmoduleService } from './authmodule.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
  JwtModule.register({
    secret: 'topSecret51',
    signOptions: {
      expiresIn: 3600,
    },
  }),
  TypeOrmModule.forFeature([AuthRepository])],
  controllers: [AuthmoduleController],
  providers: [AuthmoduleService, JwtStrategy],
  exports: [ JwtStrategy, PassportModule]
})
export class AuthmoduleModule {}
