import { IsNotEmpty, Length } from "class-validator";
import { Unique } from "typeorm";

export class createUserDto{
        @IsNotEmpty()
        @Length(4, 20)
        username: string;
        @IsNotEmpty()
        @Length(8, 20)
        password: string;
}