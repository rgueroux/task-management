import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { Auth } from "./authmodule/auth.entity";
import { AuthRepository } from "./authmodule/auth.repository";
import { JwtPayload } from "./authmodule/jwtPayload.model";
import { ExtractJwt, Strategy } from 'passport-jwt';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(AuthRepository)
        private userRepository: AuthRepository,
        ) {
            super({
                jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
                secretOrKey: 'topSecret51',
            });
        }

async validate(payload: JwtPayload): Promise<Auth> {
    const { username } = payload;
    const user = await this.userRepository.findOne({ username });
    if (!user) {
        throw new UnauthorizedException();
    }
    return user;
}
}
