import { Module } from '@nestjs/common';
import { typeormConfig } from './config/typeorm.config';
import { TasksModule } from './tasks/tasks.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthmoduleModule } from './authmodule/authmodule.module';


@Module({
  imports: [TasksModule, TypeOrmModule.forRoot(typeormConfig), AuthmoduleModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
